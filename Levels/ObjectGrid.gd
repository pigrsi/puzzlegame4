extends Node

var object_grid = {}
var grid_additions_this_turn = []

var grid_bottom

func update_grid_position(object, old, new):
	var key = vector_to_key(old)
	if object_grid.has(key) and object_grid[key] == object:
		object_grid.erase(vector_to_key(old))
	if new != null: add_to_grid(object, new)
	
	
func add_to_grid(object, position):
	object_grid[vector_to_key(position)] = object
	grid_additions_this_turn.append(object)
	
	
func remove_from_grid(object, position):
	update_grid_position(object, position, null)
	

func destroyed(object):
	grid_additions_this_turn.append(object)
	
	
func get_object_at_position(position):
	var key = vector_to_key(position)
	return null if !object_grid.has(key) else object_grid[key]
	

func get_closest_object_below(from):
	# Search down until something is found to land on
	while from.y > grid_bottom:
		from.y -= 1
		var obj = get_object_at_position(from)
		if obj != null: return obj
	return null


func are_all_objects_settled():
	for key in object_grid.keys():
		if not object_grid[key].is_settled():
			return false
	return true


func calculate_landing_positions():
	# Loop through a copy as the original will be modified
	var copy = object_grid.duplicate()
	
	for key in copy.keys():
		copy[key].calculate_landing_position()
	

func do_post_phase_grid_update():
	phase_reset()
		
		
func phase_reset():
	for key in object_grid.keys():
		object_grid[key].phase_reset()


func turn_reset():
	grid_additions_this_turn = []
	for key in object_grid.keys():
		object_grid[key].turn_reset()


func vector_to_key(position):
	return "%d,%d,%d" % [position.x, position.y, position.z]
	
