extends GridMap

const named_objects = {
	"Player": preload("res://Objects/Player/Player.tscn"),
	"Ground_0": preload("res://Objects/Ground/Ground_0.tscn"),
	"Ground_1": preload("res://Objects/Ground/Ground_1.tscn"),
	"Goal": preload("res://Objects/Goal/Goal.tscn"),
	"Wallie": preload("res://Objects/Wallie/Wallie.tscn")
}

var Player
onready var flow = get_parent()
onready var Root = get_tree().get_root()
var inventory
var grid_interface
var grid_bottom = null

func _init():
	visible = false	
	

func spawn_gridmap_objects():
	var used_cells = get_used_cells()
	for used_cell in used_cells:
		var item_id = get_cell_item(used_cell.x, used_cell.y, used_cell.z)
		var item_name = mesh_library.get_item_name(item_id)
		if grid_bottom == null or used_cell.y < grid_bottom:
			grid_bottom = used_cell.y
		if named_objects.has(item_name):
			spawn_object(item_name, used_cell, true)
			
	init_pickups()
			

func spawn_object(object_name, position, from_gridmap=false):
	var instance = named_objects[object_name].instance()
	instance.translation = position
	instance.grid = grid_interface
	instance.flow = flow
	instance.on_spawn(from_gridmap)
	Root.call_deferred("add_child", instance)
	instance.type = object_name.split('_', false, 1)[0]
	if object_name == "Player":
		Player = instance
		Player.inventory = inventory
		get_tree().call_group("Camera", "set_target", Player)
	return instance


func init_pickups():
	for instance in $Pickups.get_children():
		instance.translation -= Vector3.ONE * 0.5
		instance.grid = grid_interface
		instance.flow = flow
		instance.on_spawn(true)
		instance.type = instance.name.split('_', false, 1)[0]
		
