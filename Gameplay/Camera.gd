extends Camera

var target
var offsets = [Vector3(0, 5, 5), Vector3(5, 5, 0), Vector3(0, 5, -5), Vector3(-5, 5, 0)]
var offset_index = 0
var offset = offsets[offset_index]

func set_target(new_target):
	target = new_target

func _process(_delta):
	if !target: return
	
	look_at_from_position(target.translation + offset, target.translation, Vector3.UP)
	
func adjust_vector(vect):
	match(offset_index):
		0: return vect
		1: return Vector3(vect.z, vect.y, -vect.x)
		2: return Vector3(-vect.x, vect.y, -vect.z)
		3: return Vector3(-vect.z, vect.y, vect.x)

func _unhandled_input(event):
	if $Tween.is_active(): return
	var prev = offset_index
	if Input.is_action_just_pressed("camera_right"):
		offset_index += 1
	elif Input.is_action_just_pressed("camera_left"):
		offset_index -= 1
	else: return
	
	offset_index = posmod(offset_index, offsets.size())
			
	$Tween.interpolate_property(self, 'offset', offset, offsets[offset_index], 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
