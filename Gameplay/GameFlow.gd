extends Node

enum Phases {
	IDLE, PLAYER, AUTOMATIC, GRAVITY, UNDO
}
const INVENTORY_ID = -1
var phase = Phases.IDLE
var turns = []
onready var camera = get_tree().get_root().find_node("Camera", true, false)

func _ready():
	$SpawnMap.grid_interface = $ObjectGrid
	$Inventory.spawner = $SpawnMap	
	$SpawnMap.inventory = $Inventory
	$SpawnMap.spawn_gridmap_objects()
	$ObjectGrid.grid_bottom = $SpawnMap.grid_bottom
	$ObjectGrid.turn_reset()
	
	
func _process(delta):
	match(phase):
		Phases.UNDO:
			if $ObjectGrid.are_all_objects_settled():
				change_phase(Phases.IDLE)
		Phases.PLAYER:
			if $ObjectGrid.are_all_objects_settled():
				change_phase(Phases.GRAVITY)
		Phases.GRAVITY:
			if $ObjectGrid.are_all_objects_settled():
				add_turn()				
				change_phase(Phases.IDLE)


func _unhandled_input(_event):
	if phase != Phases.IDLE: return
	var player = $SpawnMap.Player
	
	var move_vector = null
	if Input.is_action_just_pressed("left"):
		move_vector = Vector3(-1, 0, 0)
	elif Input.is_action_just_pressed("right"):
		move_vector = Vector3(1, 0, 0)
	elif Input.is_action_just_pressed("up"):
		move_vector = Vector3(0, 0, -1)
	elif Input.is_action_just_pressed("down"):
		move_vector = Vector3(0, 0, 1)
	elif Input.is_action_just_pressed("ability"):
		$Inventory.use_ability(player.translation)
	elif Input.is_action_just_pressed("undo"):
		undo()
		return
	else: 
		return
	
	if move_vector != null:
		move_vector = camera.adjust_vector(move_vector)
		player.player_move(move_vector)

func move_started():
	if phase != Phases.IDLE: return
	
	change_phase(Phases.PLAYER)
	
	
func add_turn():
	var changes = $ObjectGrid.grid_additions_this_turn
	var turn = {}
	for object in changes:
		turn[object.get_instance_id()] = { "position": object.start_of_turn_position, "type": object.type }
		
	turn[INVENTORY_ID] = $Inventory.get_backup()
	$Inventory.clear_backup()
		
	turns.append(turn)
	
	
func undo():
	change_phase(Phases.UNDO)
	if not turns.empty():
		var turn = turns.pop_back()
			
		for key in turn.keys():
			if key == INVENTORY_ID:
				$Inventory.restore(turn[key])
			else:
				var obj = instance_from_id(key)
				var position = turn[key].position
				if position != null:
					if obj.destroyed: obj.restore()
					obj.move_to_position(position)
				else:
					obj.destroy()
					obj.queue_free()
	

func change_phase(new_phase):
	var old_phase = phase
	var keys = Phases.keys()
#	print("Phase changed from %s to %s" % [keys[old_phase], keys[new_phase]])
	
	self.phase = new_phase

	$ObjectGrid.do_post_phase_grid_update()

	match new_phase:
		Phases.IDLE:
			$ObjectGrid.turn_reset()
		Phases.GRAVITY:
			$ObjectGrid.calculate_landing_positions()
