extends "res://Objects/ObjectTemplate.gd"

export var alternate_material: Material

func _ready():
	var swap = false
	if int(translation.x + translation.z) % 2 != 0:
		swap = true
	if int(translation.y) % 2 != 0:
		swap = !swap
		
	if swap:
		$MeshInstance.material_override = alternate_material

func move_in_direction(direction) -> bool:
	return false

func apply_gravity(delta):
	pass
	
func calculate_landing_position():
	return get_grid_position()

func is_settled():
	return true
