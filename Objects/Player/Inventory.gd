extends Node

var spawner

var items = {
#	"Wallie": { "used": [], "amount": 0 }
}
var active_ability = "Wallie"
var turn_backup = {}


func use_ability(player_translation):
	if not active_ability or not items.has(active_ability): return
	
	backup_ability(active_ability)
	
	var ability = items[active_ability]
	
	if ability.amount > 0:
		ability.amount -= 1
		var spawned = spawner.spawn_object(active_ability, player_translation)
		ability.used.append(spawned)
	elif not ability.used.empty():
		var oldest = ability.used.pop_front()
		oldest.reuse_ability(player_translation)
		ability.used.append(oldest)



func pickup(pickup):
	var ability = pickup.ability_name
	backup_ability(ability)
	
	if not items.has(ability):
		items[ability] = { "used": [], "amount": 0 }
	items[ability].amount += pickup.value


func backup_ability(ability):
	if turn_backup.has(ability): return
	if items.has(ability): 
		turn_backup[ability] = {
			"used": [] + items[ability].used,
			"amount": items[ability].amount
		}
	else:
		turn_backup[ability] = {
			"used": [],
			"amount": 0
		}
	
	
func get_backup():
	return turn_backup
	
	
func restore(backup):
	for key in backup.keys():
		items[key] = backup[key]


func clear_backup():
	turn_backup = {}
