extends "res://Objects/ObjectTemplate.gd"

signal picked_up(pickup)

var inventory

func player_move(move_vector):
	move_in_direction(move_vector)
		
	
func move_to_position(position):
	if .move_to_position(position):
		flow.move_started()
		return true
	return false
	
	
func move_in_direction(move_vector) -> bool:
	var new_position = transform.origin + move_vector
	var blocking_object = grid.get_object_at_position(new_position)
	if blocking_object and "Pickup" in blocking_object.type:
		inventory.pickup(blocking_object)
		blocking_object.on_pickup()
	
	return .move_in_direction(move_vector)
