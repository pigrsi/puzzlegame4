extends Spatial

var grid
var type
var flow
var has_moved = false
var falling_to
var start_of_turn_position
var destroyed = false


func _process(delta):
	visible = !destroyed

func on_spawn(from_gridmap):
	if not from_gridmap:
		displace_object(translation)
			
	grid.add_to_grid(self, translation)


func displace_object(position):
	var existing = grid.get_object_at_position(position)
	if existing != null:
		existing.move_in_direction(Vector3.UP)


func move_to_position(position):
	grid.update_grid_position(self, translation, position)
	$MovementTween.interpolate_property(self, "translation", translation, 
	position, 0.05, Tween.TRANS_LINEAR , Tween.EASE_IN_OUT)
	$MovementTween.start()
	has_moved = true
	return true
	
	
func move_in_direction(move_vector) -> bool:
	var new_position = transform.origin + move_vector

	# Try pushing if something is in the way
	var can_move = true
	var blocking_object = grid.get_object_at_position(new_position)
	if blocking_object:
		can_move = blocking_object.move_in_direction(move_vector)
			
	if can_move:
		# Move object on top
		var above = grid.get_object_at_position(translation + Vector3.UP)
		move_to_position(new_position)
		if above: above.move_in_direction(move_vector)
		
	return can_move


func destroy():
	grid.remove_from_grid(self, translation)
	grid.destroyed(self)
	destroyed = true
	
	
func restore():
	destroyed = false
	
	
func phase_reset():
	has_moved = false
	falling_to = null
	
	
func turn_reset():
	start_of_turn_position = translation
	
	
func get_grid_position():
	return translation
		
		
func calculate_landing_position():
	if falling_to: return falling_to
	
	var landing_object = grid.get_closest_object_below(translation)
	if landing_object == null: 
		start_fall(Vector3(translation.x, translation.y-4, translation.z))
		return Vector3(translation.x, translation.y-4, translation.z)
		
	var land_on_position = landing_object.calculate_landing_position()
	var new_position = land_on_position + Vector3.UP
	start_fall(new_position)
	return new_position
	
	
func start_fall(new_position):
	falling_to = new_position
	if new_position == translation: return
	grid.update_grid_position(self, translation, new_position)
	var distance = translation.y - new_position.y
	$MovementTween.interpolate_property(self, "translation", translation, 
	new_position, sqrt((2 * distance) / 20), Tween.TRANS_SINE , Tween.EASE_IN)
	$MovementTween.start()

	
func is_settled():
	return !$MovementTween.is_active()
	
